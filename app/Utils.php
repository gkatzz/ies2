<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Utils extends Model
{
    protected $user;
    protected $grade;
    protected $redirectPath;
    protected $redirectRoute;
    protected $profileName;

    public function __construct(){
        $this->user = new User();
        $this->grade = new Grade();
    }

    public function returnData(){
        $token = Session::get('token');
        $userId = $token['result']['userId'];

        $this->profileName = $token['result']['userProfile'];
        $this->user = $this->user->findUserById($userId);
        $this->grade = $this->grade->listGradesByUser($userId);

        $this->redirectPath = '/dashboard/';
        $this->redirectRoute = 'dashboard';

        $data = [
            'user' => $this->user,
            'grades' => $this->grade,
            'profileName' => $this->profileName,
            'redirectPath' => $this->redirectPath,
            'redirectRoute' => $this->redirectRoute
        ];

        return $data;
    }

    public function uploadImageFile($img, $personType, $userDocumentNumber, $fileType = null){
        if ($img->isValid()) {

            $destinationPath = 'custom/img/'.$personType.'/'.$userDocumentNumber;
            if (!is_dir($destinationPath) && !mkdir($destinationPath)){
                $data['error'] = "Ocorreu um erro ao realizar o upload da imagem";
                return back()->withErrors($data['error']);
            }

            $extension = $img->getClientOriginalExtension();
            $fileName = rand(11111, 99999).'.'.$extension;
            if(!is_null($fileType))
                $fileName = $fileType.'.'.$extension;

            $files = glob($destinationPath.'/*');
            foreach($files as $file){
                if((basename($file) == $fileType) && is_file($file))
                    unlink($fileType);
            }

            $img->move($destinationPath, $fileName);

            return $destinationPath.'/'.$fileName;
        } else {
            return false;
        }
    }
}
