<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{

    protected $table = 'usersToken';

    protected $primaryKey = 'usersTokenId';

    public $timestamps = false;

    protected $dates = ['expire'];

    public function __construct(){
        //
    }

    public function findTokenExpired($user){
        return $this
                ->where('userId', $user->userId)
                ->where('expire', '>=', Carbon::now())
                ->get()
                ->first();
    }
}
