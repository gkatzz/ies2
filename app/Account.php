<?php
namespace App;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Carbon\Carbon;

class Account
{
    protected $user;
    protected $userToken;

    public function __construct(User $user, UserToken $userToken)
    {
        $this->user = $user;
        $this->userToken = $userToken;
    }

    public function login($request){
        $login = $request->login;

        $user = $this->user
            ->join('profile', 'user.profileId', '=', 'profile.profileId')
            ->where('user.login', $login)
            ->addSelect(
                'user.name as userName', 'profile.name as profileName',
                'user.*', 'profile.*'
            )
            ->get()
            ->first();

        if((!is_null($user)) && Hash::check($request->password, $user->password)){
            $findTokenUser = $this->userToken->findTokenExpired($user);

            if(!is_null($findTokenUser)){
                $data['result'] = [
                    'userId' => $user->userId,
                    'userProfile' => $user->profileName,
                    'name' => $user->name,
                    'token' => $findTokenUser->token,
                    'email' => $user->email,
                    'login' => $user->login,
                    'expiresOn' => $findTokenUser->expire
                ];
            } else {
                $hash = hash('sha256', Str::random(10), false);
                $datetime_expire = Carbon::now()->addHour(5);

                $userToken = new userToken();

                $userToken->userId = $user->userId;
                $userToken->token = $hash;
                $userToken->expire = $datetime_expire;

                $userToken->save();

                $data['result'] = [
                    'userId' => $userToken->userId,
                    'userProfile' => $user->profileName,
                    'name' => $user->name,
                    'token' => $userToken->token,
                    'email' => $user->email,
                    'login' => $user->login,
                    'expiresOn' => $userToken->expire
                ];
            }

            Session::put('token', $data);

            $data = ['messageError' => '', 'resp' => true];

        } else {
            $data = ['messageError' => 'Usuário ou senha incorreto', 'resp' => false];
        }
        return $data;
    }

    public function alterPasswordUser($request,$userId){
        $user = $this->user
            ->where('userId', $userId)
            ->get()
            ->first();

        $user->password = Hash::make($request->newPassword);
        $user->update();

        return $user;
    }
}
