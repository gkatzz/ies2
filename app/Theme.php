<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class Theme extends Model
{
    protected $table = 'theme';

    protected $primaryKey = 'themeId';

    public $timestamps = false;

    public function __construct(){
        //
    }

    public function listThemes(){
        $themes = $this
            ->addSelect(
                'theme.name as themeName',
                'theme.*'
            )
            ->get();

        return $themes;
    }

    public function findThemeById($themeId){
        $theme = $this
            ->where('theme.themeId', $themeId)
            ->get()
            ->first();
        return $theme;
    }
}
