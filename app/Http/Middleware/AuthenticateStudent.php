<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ApiController;
use Closure;
use Illuminate\Support\Facades\Session;

class AuthenticateStudent
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = Session::get('token');

        if (!is_null($token)) {
            $token = $token['result']['token'];

            if (ApiController::searchTokenValidUser($token)) {
                $token = Session::get('token');
                $profileName = $token['result']['userProfile'];

                if ($profileName == 'Aluno') {
                    return $next($request);
                } else {
                    Session::flush();
                    return redirect('/');
                }

            } else {
                Session::flush();
                return redirect('/');
            }
        }
    }
}
