<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ApiController;
use Closure;
use Illuminate\Support\Facades\Session;

class AuthenticateAll
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = Session::get('token');

        if (!is_null($token))
        {
            $token = $token['result']['token'];

            if (ApiController::searchTokenValidUser($token)) {
                    return $next($request);
            }else{
                Session::flush();
                return redirect('/');
            }
        }

        return redirect('/');
    }
}
