<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ApiController;
use Closure;
use Illuminate\Support\Facades\Session;

class AuthenticateTeacher
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = Session::get('token');

        if (!is_null($token))
        {
            $token = $token['result']['token'];

            if (ApiController::searchTokenValidUser($token)) {
                $token = Session::get('token');
                $profileName = $token['result']['userProfile'];

                if ($profileName == 'Professor') {
                    return $next($request);
                } else {
                    Session::flush();
                    return redirect('/');
                }

            }else{
                Session::flush();
                return redirect('/');
            }
        }

        return redirect('/');
    }
}
