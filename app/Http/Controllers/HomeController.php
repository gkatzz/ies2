<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use App\User;

class HomeController extends ApiController
{

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $token = Session::get('token');
        if(isset($token['result']['rememberMe'])){
            if(!is_null($token['result']['rememberMe'])){
                $userId = $token['result']['userId'];
                $user = $this->user->findUserById($userId);
                $data = [
                    'user' => $user
                ];
                return View('dashboard.dashboard')->with(['data' => $data]);
            }
        }
        return View('home.index');
    }

}
