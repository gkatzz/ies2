<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\User;
use App\Account;

class LoginController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $user;
    protected $account;

    public function __construct(User $user, Account $account)
    {
        $this->user = $user;
        $this->account = $account;
    }

    public function login(Request $request)
    {
        $login = $this->account->login($request);
        if($login['resp']){
            $token = Session::get('token');
            $userId = $token['result']['userId'];
            $user = $this->user->findUserById($userId);
            if($user)
                return redirect('/dashboard');
        }

        $v = Validator::make($request->all(), []);
        return redirect('/')->withErrors($v->errors()->add('field', $login['messageError']));
    }

    public function logout()
    {
        Session::flush();
        return redirect('/');
    }
}
