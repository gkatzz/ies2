<?php

namespace App\Http\Controllers;
use App\Grade;
use App\Theme;
use App\User;
use App\Utils;
use Illuminate\Http\Request;

class GradeController extends ApiController
{
    protected $utils;
    protected $grade;

    public function __construct( Utils $utils){
        $this->grade = new Grade();
        $this->utils = new Utils();
    }

    public function index()
    {
        $data = $this->utils->returnData();
        $grades = $this->grade->listGradesByUser($data['user']->userId);
        $data['grades'] = $grades;
        return View('dashboard.grade.index')->with(['data' => $data]);
    }

    public function indexTheme()
    {
        $data = $this->utils->returnData();
        $grades = $this->grade->listGradesByTheme($data['user']->userId);
        $data['grades'] = $grades;
        return View('dashboard.grade.index')->with(['data' => $data]);
    }

    public function create(){
        $data = $this->utils->returnData();
        $students = new User();
        $themes = new Theme();

        $students = $students->listStudents();
        $themes = $themes->listThemes();

        $data['students'] = $students;
        $data['themes'] = $themes;

        return View('dashboard.grade.create')->with(['data' => $data]);
    }

    public function edit($gradeId){
        $data = $this->utils->returnData();
        $students = new User();
        $themes = new Theme();

        $grade = $this->grade->findGradeById($gradeId);
        $students = $students->listStudents();
        $themes = $themes->listThemes();

        $data['students'] = $students;
        $data['themes'] = $themes;
        $data['grade'] = $grade;

        return View('dashboard.grade.edit')->with(['data' => $data]);
    }

    public function store(Request $request){
        $data = $this->utilsreturnData();

        $storedGrade = $this->grade->storeGrade($request);

        $messageTitle = "create-success";

        if(!$storedGrade)
            $messageTitle = "create-error";

        return redirect()->route($this->utils->redirectRoute)->with($messageTitle, $data);
    }

    public function update(Request $request){
        $request->gradeId = $request->hdnGradeId;
        $data = $this->returnData();

        $storedGrade = new Grade();

        $saveGrade= $this->grade->updateGrade($storedGrade, $request);

        $messageTitle = "edit-success";

        $data["grade"] = $saveGrade;

        if(!$saveGrade)
            $messageTitle = "edit-error";

        return redirect()->route($this->utils->redirectRoute)->with($messageTitle, $data);
    }

    public function delete(Request $request){
        $deleteGrade = $this->grade->deleteGrade($request->gradeId);

        $data['error'] = true;
        if($deleteGrade)
            $data['error'] = false;
        return response()->json($data);
    }

}
