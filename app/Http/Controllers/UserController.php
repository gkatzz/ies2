<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Utils;

class UserController extends ApiController
{
    protected $utils;

    public function __construct( Utils $utils){
        $this->utils = new Utils();
    }

    public function index()
    {
        $data = $this->utils->returnData();

        return View('dashboard')->with(['data' => $data]);
    }

    public function checkExistingLogin(Request $request)
    {
        $user = $this->user->checkExistingLogin($request->login);
        return json_encode($user);
    }


    public function checkExistingLoginByUser(Request $request)
    {
        $user = $this->user->checkExistingLoginByUser($request->userId, $request->login);
        return json_encode($user);
    }

    public function checkExistingEmail(Request $request)
    {
        $user = $this->user->checkExistingEmail($request->email);
        return json_encode($user);
    }

    public function checkExistingEmailByUser(Request $request)
    {
        $user = $this->user->checkExistingEmailByUser($request->userId, $request->email);
        return json_encode($user);
    }
}
