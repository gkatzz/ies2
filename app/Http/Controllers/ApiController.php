<?php
namespace App\Http\Controllers;

use App\UserToken;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class ApiController extends Controller
{

	public static function searchTokenValid($token)
	{
		return null;
	}

    public static function searchTokenValidUser($token)
    {
        $findTokenUser = UserToken::where('token', $token)->where('expire', '>=', Carbon::now())->get()->first();
        return !is_null($findTokenUser);
    }

	protected function showToken($token)
	{
        return null;
	}

    protected function checkBody($request)
	{
		if (empty($request->all()))
		{
			return response()->json(trans('api.400'), 400);
		}
	}

	public function returnSomeRequest($ret)
	{
        return null;
	}

	protected function validateRequest($request, $rules)
	{
		$v = Validator::make($request->all(), $rules);
		if ($v->fails())
		{
			return $v->errors()->all();
		} else
		{
			return true;
		}
	}
}