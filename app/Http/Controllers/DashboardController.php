<?php

namespace App\Http\Controllers;
use App\Grade;
use Illuminate\Support\Facades\Session;
use App\User;

class DashboardController extends ApiController
{
    protected $user;
    protected $grade;
    protected $member;

    public function __construct(User $user, Grade $grade)
    {        
        $this->user = $user;
        $this->grade = $grade;
    }

    public function index()
    {
        $token = Session::get('token');
        if(!is_null($token)){
            $userId = $token['result']['userId'];

            $user = $this->user->findUserById($userId);
            $grade = $this->grade->listGradesByUser($userId);
            $member = null;

            $data = [
                'user' => $user,
                'grade' => $grade
            ];

            return View('dashboard.index')->with(['data' => $data]);
        }

        return redirect('/');
    }

}
