<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class User extends Model
{
    use Notifiable;

    protected $table = 'user';

    protected $primaryKey = 'userId';

    public $timestamps = false;

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function __construct(){
        //
    }

    public function listUsers(){
        $user = $this
            ->join('profile', 'user.profileId', '=', 'profile.profileId')
            ->addSelect(
                'user.name as userName', 'profile.name as profileName',
                'user.*', 'profile.*'
            )
            ->get();
        return $user;
    }

    public function listStudents(){
        $user = $this
            ->join('profile', 'user.profileId', '=', 'profile.profileId')
            ->where('profile.profileId', 2)
            ->addSelect(
                'user.name as studentName', 'profile.name as profileName',
                'user.*', 'profile.*'
            )
            ->get();
        return $user;
    }

    public function findUserById($userId){
        $user = $this
            ->join('profile', 'user.profileId', '=', 'profile.profileId')
            ->where('user.userId', '=', $userId)
            ->addSelect(
                'user.name as userName', 'profile.name as profileName',
                'user.*', 'profile.*'
            )
            ->get()
            ->first();
        return $user;
    }

    public function findUserByLogin($login){
        $user = $this
        ->join('profile', 'user.profileId', '=', 'profile.profileId')
            ->where('user.login', '=', $login)
            ->addSelect(
                'user.name as userName', 'profile.name as profileName',
                'user.*', 'profile.*'
            )
            ->get()
            ->first();
        return $user;
    }

    public function findUserByEmail($email){
        $user = $this
            ->join('profile', 'user.profileId', '=', 'profile.profileId')
            ->where('user.email', '=', $email)
            ->addSelect(
                'user.name as userName', 'profile.name as profileName',
                'user.*', 'profile.*'
            )
            ->get()
            ->first();
        return $user;
    }

    public function storeUser($request){
        $user = $this;

        $user->profileId = $request->profileId;
        $user->name = $request->name != '' ? $request->name : null;
        $user->login = $request->login != '' ? $request->login : null;
        $user->email = $request->email != '' ? $request->email : null;
        $user->password = Hash::make($request->password);
        $user->createdAt = Carbon::now();
        $user->createdBy = isset(Session::get('token')['result']['UserId']) ? Session::get('token')['result']['UserId'] : null;

        if(isset($request->profilePicture) && !is_null($request->profilePicture)){
            $utils = new Utils();
            $profileImgPath = $utils->uploadImageFile($request->profilePicture, 'user', $request->login, 'profile-picture');
            $user->profilePicture = $profileImgPath;
        }

        $user->save();

        return $user;
    }

    public function updateUser($request){
        $user = $this->findUserById($request->userId);

        $user->name = $request->name != '' ? $request->name : null;
        $user->name = $request->name != '' ? $request->name : null;
        $user->login = $request->login != '' ? $request->login : null;
        $user->profileId = $request->profileId == '' ? $user->profileId : $request->profileId;
        $user->email = $request->email != '' ? $request->email : null;
        $user->password = Hash::make($request->password);
        $user->updatedAt = Carbon::now();
        $user->updatedBy = isset(Session::get('token')['result']['UserId']) ? Session::get('token')['result']['UserId'] : null;

        if($request->alterPassword != ''){
            $user->Password = Hash::make($request->alterPassword);
        }
        if(isset($request->profilePicture) && !is_null($request->profilePicture)){
            $utils = new Utils();
            $profileImgPath = $utils->uploadImageFile($request->profilePicture, 'user', $request->login, 'profile-picture');
            $user->profilePicture = $profileImgPath;
        }

        $user->update();

        return $user;
    }

    public function deleteUser($request){
        $user = $this->findUserById($request->userId);
        $user->delete();

        $data['error'] = true;
        if($user)
            $data['error'] = false;
        return response()->json($data);
    }

    public function checkExistingLogin($login){
        $data['result'] = false;
        $user = $this->findUserByLogin($login);
        if(!is_null($user))
            $data['result'] = true;
        return $data['result'];
    }

    public function checkExistingLoginByUser($userId, $login){
        $data['result'] = false;

        $user = $this
            ->join('profile', 'user.profileId', '=', 'profile.profileId')
            ->where('user.login', '=', trim($login))
            ->where('user.userId', '<>', $userId)
            ->addSelect(
                'user.name as userName', 'profile.name as profileName',
                'user.*', 'profile.*'
            )
            ->get()
            ->first();

        if(!is_null($user))
            $data['result'] = true;
        return $data['result'];
    }

    public function checkExistingEmail($email){
        $data['result'] = false;
        $user = $this->findUserByEmail($email);
        if(!is_null($user))
            $data['result'] = true;
        return $data['result'];
    }

    public function checkExistingEmailByUser($userId, $email){
        $data['result'] = false;
        $user = $this
            ->join('profile', 'user.profileId', '=', 'profile.profileId')
            ->where('user.email', '=', $email)
            ->where('user.userId', '<>', $userId)
            ->addSelect(
                'user.name as userName', 'profile.name as profileName',
                'user.*', 'profile.*'
            )
            ->get()
            ->first();

        if(!is_null($user))
            $data['result'] = true;
        return $data['result'];
    }
}
