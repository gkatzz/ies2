<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class Grade extends Model
{
    protected $table = 'grade';

    protected $primaryKey = 'gradeId';

    public $timestamps = false;

    public function __construct(){
        //
    }

    public function listGradesByUser($userId){
        $grades = $this
            ->join('user as u1', 'u1.userId', '=', 'grade.studentId')
            ->join('user as u2', 'u2.userId', '=', 'grade.teacherId')
            ->where('grade.teacherId', $userId)
            ->orWhere('grade.studentId', $userId)
            ->addSelect(
                'u1.name as userName',
                'u1.*', 'grade.*'
            )
            ->get();

        return $grades;
    }

    public function listGradesByTheme($userId){
        $grades = $this
            ->join('user as u1', 'u1.userId', '=', 'grade.studentId')
            ->join('user as u2', 'u2.userId', '=', 'grade.teacherId')
            ->join('theme', 'theme.gradeId', '=', 'grade.gradeId')
            ->where('grade.teacherId', $userId)
            ->orWhere('grade.studentId', $userId)
            ->addSelect(
                'u1.name as userName',
                'theme.name as themeName',
                'u1.*', 'theme.*', 'grade.*'
            )
            ->get();

        return $grades;
    }

    public function findGradeById($gradeId){
        $grade = $this
            ->where('grade.gradeId', $gradeId)
            ->get()
            ->first();
        return $grade;
    }

    public function storeGrade($request){
        $grade = $this;

        $grade->teacherId = $request->teacherId;
        $grade->studentId = $request->studentId;
        $grade->grade = $request->grade;

        $grade->save();

        return $grade;
    }

    public function updateGrade($request){
        $gradeId = $request->hdnGradeId;

        $grade = $this->findGradeById($gradeId);

        $grade->teacherId = $request->hdnTeacherId;
        $grade->studentId = $request->hdnStudentId;
        $grade->grade = $request->grade;

        $grade->update();

        return $grade;
    }

    public function deleteGrade($gradeId){
        $grade = $this->findGradeById($gradeId);
        $user = new User();
        $user = $user->findUserByGrade($gradeId);
        $user->delete();
        $grade->delete();

        return $grade;
    }

    public function deleteUserGrade($gradeId){
        $grade = $this->listGradesByUser($gradeId);
        $grade->delete();

        return $grade;
    }
}
