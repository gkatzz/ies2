<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function(Blueprint $table)
        {
            $table->increments('profileId');
            $table->string('name', 50);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();
        });

        Schema::create('user', function (Blueprint $table) {
            $table->increments('userId');
            $table->integer('profileId')->unsigned();
            $table->foreign('profileId')
                ->references('profileId')->on('profile')
                ->onDelete('cascade');
            $table->string('name', 50);
            $table->string('email')->unique();
            $table->string('login', 20)->unique();
            $table->string('password');
            $table->string('profilePicture', 250)->nullable()->default('images/users/0/user-default.png');
            $table->rememberToken();
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();
        });
        Schema::create('usersToken', function (Blueprint $table) {
            $table->increments('userTokenId');
            $table->integer('userId')->unsigned();
            $table->foreign('userId')
                ->references('userId')->on('user')
                ->onDelete('cascade');
            $table->string('token', 100);
            $table->timestamp('expire')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('createdAt')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usersToken', function ($table) {
            $table->dropForeign(['userId']);
        });
        Schema::drop('usersToken');

        Schema::table('user', function ($table) {
            $table->dropForeign(['profileId']);
        });
        Schema::dropIfExists('user');

        Schema::dropIfExists('profile');
    }
}
