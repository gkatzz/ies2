<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grade', function (Blueprint $table) {
            $table->increments('gradeId');
            $table->integer('teacherId')->unsigned();
            $table->foreign('teacherId')
                ->references('userId')->on('user')
                ->onDelete('cascade');
            $table->integer('studentId')->unsigned();
            $table->foreign('studentId')
                ->references('userId')->on('user')
                ->onDelete('cascade');
            $table->decimal('grade', 4,2);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();
        });
        Schema::create('theme', function (Blueprint $table) {
            $table->increments('themeId');
            $table->integer('gradeId')->unsigned();
            $table->foreign('gradeId')
                ->references('gradeId')->on('grade')
                ->onDelete('cascade');
            $table->string('name');
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('theme', function ($table) {
            $table->dropForeign(['gradeId']);
        });
        Schema::drop('theme');
        Schema::table('grade', function ($table) {
            $table->dropForeign(['teacherId']);
            $table->dropForeign(['studentId']);
        });
        Schema::drop('grade');
    }
}
