<?php

use Illuminate\Database\Seeder;

class seed_user_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Professor'
            ],
            [
                'name' => 'Aluno'
            ]
        ];

        DB::table('profile')->insert($data);

        $data = [
            [
                'profileId' => 1,
                'name' => 'Professor Teste 1',
                'email' => 'professor1@domain.com',
                'login' => 'professor1',
                'password' => \Illuminate\Support\Facades\Hash::make("teste_professor1")
            ],
            [
                'profileId' => 2,
                'name' => 'Aluno Teste 1',
                'email' => 'aluno1@domain.com',
                'login' => 'aluno1',
                'password' => \Illuminate\Support\Facades\Hash::make("teste_aluno1")
            ],
            [
                'profileId' => 1,
                'name' => 'Professor Teste 2',
                'email' => 'professor2@domain.com',
                'login' => 'professor2',
                'password' => \Illuminate\Support\Facades\Hash::make("teste_professor2")
            ],
            [
                'profileId' => 2,
                'name' => 'Aluno Teste 2',
                'email' => 'aluno2@domain.com',
                'login' => 'aluno2',
                'password' => \Illuminate\Support\Facades\Hash::make("teste_aluno2")
            ]
        ];

        DB::table('user')->insert($data);
    }
}
