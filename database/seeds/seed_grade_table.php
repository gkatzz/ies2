<?php

use Illuminate\Database\Seeder;

class seed_grade_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [   'grade' => 1,
                'teacherId' => 1,
                'studentId' => 2
            ],
            [
                'grade' => 1.5,
                'teacherId' => 1,
                'studentId' => 4
            ],
            [
                'grade' => 2,
                'teacherId' => 3,
                'studentId' => 2
            ],
            [
                'grade' => 2.5,
                'teacherId' => 3,
                'studentId' => 4
            ]
        ];

        DB::table('grade')->insert($data);

        $data = [
            [   'name' => 'Português',
                'gradeID' => 1
            ],
            [   'name' => 'Cálculo',
                'gradeID' => 2
            ],
            [   'name' => 'Biologia',
                'gradeID' => 3
            ],
            [   'name' => 'Física',
                'gradeID' => 4
            ]
        ];

        DB::table('theme')->insert($data);
    }
}
