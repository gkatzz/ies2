@extends('errors::layout')

@section('title', 'Page Not Found')

@section('message', 'Desculpe, a página que você procura não foi encontrada.')
