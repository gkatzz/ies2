<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::post('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::group(['prefix' => 'dashboard', 'middleware' => 'access.all'], function () {

    Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('/grade', ['as' => 'dashboard.grade.list', 'uses' => 'GradeController@index']);
    Route::get('/grade/theme/{id}', ['as' => 'dashboard.grade.theme.list', 'uses' => 'GradeController@indexTheme']);

    Route::group(['prefix' => 'grade', 'middleware' => 'access.teacher'], function () {
        Route::get('/create', ['as' => 'dashboard.grade.create', 'uses' => 'GradeController@create']);
        Route::get('/edit/{gradeId}', ['as' => 'dashboard.grade.edit', 'uses' => 'GradeController@edit']);
        Route::post('/update', ['as' => 'dashboard.grade.update', 'uses' => 'GradeController@update']);
        Route::post('/store', ['as' => 'dashboard.grade.store', 'uses' => 'GradeController@store']);
        Route::delete('/delete', ['as' => 'dashboard.grade.delete', 'uses' => 'GradeController@delete']);
    });
});