@extends('layouts.home')
@section('content')
    <form method="POST" name="login-form" id="login-form" action="login" class="col-md-5 col-sm-5 col-xs-11 center-margin">
        {{csrf_field()}}
        <div id="login-form" class="content-box">
            <div class="login-centered">
                {{ Form::image(asset('images/logo.png'), 'frm-img-logo', ['id' => 'frm-img-logo', 'class' => 'frm-logo-img']) }}
                @if (count($errors) > 0)
                    <div class="alert alert-danger login-alert content-box-wrapper">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="content-box-wrapper content-box-wrapper-padding">
                    <div class="form-group">
                        <label for="login">Usuário:</label>
                        <div class="input-group input-group-md">
                            <span class="input-group-addon addon-inside">
                                <i class="fa fa-user frm-login-icon"></i>
                            </span>
                                <input type="text" class="form-control frm-input-login" id="login" name="login" placeholder="Login">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Senha:</label>
                        <div class="input-group input-group-md">
                        <span class="input-group-addon addon-inside bg-white">
                            <i class="fa fa-lock frm-login-icon"></i>
                        </span>
                            <input type="password" class="form-control frm-input-login" name="password" id="password" placeholder="Senha">
                        </div>
                    </div>
                </div>
                <div class="button-pane btn-pane-top-line text-center">
                    <div class="text-center">
                        <button type="submit" class="btn frm-btn-login">Entrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop