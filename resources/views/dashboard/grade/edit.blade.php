@extends('layouts.master')
@section('dashboard')
    @include('layouts.menu')
    @if (count($errors) > 0)
        <div class="panel">
            <div class="panel-body">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Editar Nota</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form role="form" method="POST" enctype="multipart/form-data" name="form-garde" id="form-grade" action="{{route('dashboard.grade.store')}}">
                        {{csrf_field()}}
                        <input type="hidden" class="form-control" name="teacherId" id="teacherId" value="{{$data['user']->userId}}" />
                        <input type="hidden" class="form-control" name="gradeId" id="gradeId" value="{{$data['grade']->userId}}" />
                        <div class="panel">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label class="col-sm-2 control-label">Aluno</label>
                                        <div class="col-sm-8">
                                            <select id="studentId" name="categoryId" class="categoryId select-custom col-md-6 pull-left">
                                                @if(count($data["students"]) > 0)
                                                    @foreach($data["students"] as $student)
                                                        @if($student->studentId == $data["grade"]->studentId)
                                                            <option selected value="{{$student->studentId}}">{{$student->studentName}}</option>
                                                        @else
                                                            <option value="{{$student->studentId}}">{{$student->studentName}}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <option value="">Nenhum Aluno cadastrada</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-sm-2 control-label">Matéria</label>
                                        <div class="col-sm-8">
                                            <select id="studentId" name="categoryId" class="categoryId select-custom col-md-6 pull-left">
                                                @if(count($data["themes"]) > 0)
                                                    @foreach($data["themes"] as $theme)
                                                        @if($student->studentId == $data["grade"]->studentId)
                                                            <option selected value="{{$data['theme']->themeId}}">{{$theme->themeName}}</option>
                                                        @else
                                                            <option value="{{$theme->themeId}}">{{$theme->themeName}}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <option value="">Nenhuma Matéria cadastrada</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label class="col-sm-2 control-label">Nota</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control numeric onlyNumbers" id="grade" name="grade" placeholder="0 - 10" maxlength="5" value="{{$data["grade"]->grade}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h3 class="page-header"></h3>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <a type="button" href="{{route('dashboard.grade.list')}}" class="btn btn-gray">Cancelar</a>
                                    </div>
                                    <div class="col-sm-6">
                                        <button id="btn-save" type="button" class="btn-save btn btn-primary">Salvar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop