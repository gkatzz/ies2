@extends('layouts.master')
@section('dashboard')
    @include('layouts.menu')
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Listagem de Notas</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col col-xs-6">
                                    <h3 class="panel-title">Notas por Aluno</h3>
                                </div>
                                <div class="col col-xs-6 text-right">
                                    <a href="{{route('dashboard.grade.create')}}" type="button" class="btn btn-sm btn-primary btn-create">Criar Nota</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-list">
                                <thead>
                                <tr>
                                    <th class="edit-col"><em class="fa fa-cog"></em></th>
                                    <th class="hidden-xs">ID</th>
                                    @if(isset($data['grades'][0]->themeName))
                                        <th>Matéria</th>
                                    @endif
                                    <th>Aluno</th>
                                    <th>Email</th>
                                    <th>Nota</th>
                                    <th>Data de Criação</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['grades'] as $grade)
                                    <tr>
                                        <td align="center">
                                            <a href="{{route('dashboard.grade.edit', $grade->gradeId)}}" class="btn btn-default"><em class="fa fa-pencil"></em></a>
                                            <a href="{{route('dashboard.grade.delete', $grade->gradeId)}}" class="btn btn-danger"><em class="fa fa-trash"></em></a>
                                        </td>
                                        <td class="hidden-xs">{{$grade->userId}}</td>
                                        @if(isset($grade->themeName))
                                            <td>{{$grade->themeName}}</td>
                                        @endif
                                        <td>{{$grade->userName}}</td>
                                        <td>{{$grade->email}}</td>
                                        <td>{{$grade->grade}}</td>
                                        <td>{{date('d/m/Y', strtotime($grade->createdAt))}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col col-xs-4">Página 1 of 5
                                </div>
                                <div class="col col-xs-8">
                                    <ul class="pagination hidden-xs pull-right">
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                    </ul>
                                    <ul class="pagination visible-xs pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop