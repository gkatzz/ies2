@extends('layouts.master')
@section('dashboard')
    @include('layouts.menu')
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">@yield('title')</h3>
                </div>
            </div>
            <div class="row">
                @yield('content')
            </div>
    </div>
@stop