@extends('layouts.master')
<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="div-logo-menu">
        <img class="menu-picture" src="{{asset("images/logo.png")}}" alt="Imagem de Perfil">
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <div class="dropdown">
            <div class="dropdown-button dropdown-toggle" data-toggle="dropdown">
                <img class="dropdown-picture" src="{{asset($data['user']->profilePicture)}}" alt="Imagem de Perfil">
                <span class="user-name-top-link">{{$data['user']->userName}}<span class="caret caret-dropdown"></span></span>
            </div>

            <ul class="dropdown-menu" role="menu" aria-labelledby="menu">
                <div class="user-info">
                    <span>
                        {{$data['user']->userName}}
                        @if($data['user']->profileId == 1)
                            <i>Professor</i>
                        @elseif($data['user']->profileId == 2)
                            <i>Aluno</i>
                        @endif
                    </span>
                </div>
                <li role="presentation"><a href="{{route('logout')}}">Sair</a></li>
            </ul>
        </div>

        <div class="user-account-btn dropdown">
            <div class="dropdown-menu">
                <div class="box-sm">
                    <div class="login-box clearfix dropdown-toggle">

                    </div>
                    <div class="button-pane button-pane-alt pad5L pad5R text-center">
                        <a href="{{route('logout')}}" class="btn btn-flat display-block font-normal btn-danger">
                            <i class="glyph-icon icon-power-off"></i>
                            Sair
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </ul>

    <div class="nav-side-menu sidebar">
        <div class="menu-list">
            <ul id="menu-content" class="menu-content collapse out">
                <li>
                    <a href="{{route('dashboard')}}">
                        <i class="fa fa-home fa-lg"></i> Home
                    </a>
                </li>
                <li data-toggle="collapse" data-target="#grades">
                    <a href="#"><i class="fa fa-bookmark fa-lg"></i> Notas <span class="arrow"></span></a>
                </li>
                <ul class="collapse" id="grades">
                    @if($data['user']->profileId == 1)
                        <a href="{{route('dashboard.grade.list')}}"><li>Por Aluno</li></a>
                    @else
                        <a href="{{route('dashboard.grade.list')}}"><li>Por Professor</li></a>
                    @endif
                    <a href="{{route('dashboard.grade.theme.list', 1)}}"><li>Por Matéria</li></a>
                </ul>
            </ul>
        </div>
    </div>
</nav>
