<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> IES2 - Sistema de Notas</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" type="image/ico" href="{{asset("favicon.ico")}}">

    <link rel="stylesheet" type="text/css" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("font-awesome/font-awesome.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("css/content-box.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("css/default.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("css/home.css")}}">
</head>
<body>
    @yield('home')
    @yield('dashboard')
</body>

<script src="{{asset("js/jquery-3.2.1.min.js")}}"></script>
<script src="{{asset("js/jquery.mask.min.js")}}"></script>
<script src="{{asset("js/validate.js")}}"></script>
<script src="{{asset("js/jquery.maskMoney.js")}}"></script>
<script src="{{asset("js/default.js")}}"></script>
<script src="{{asset("bootstrap/js/bootstrap.min.js")}}"></script>
</html>