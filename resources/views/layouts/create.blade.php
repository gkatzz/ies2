@extends('layouts.master')
@include('includes.css.create')
@section('body')
    @yield('modal')
    <div class="panel panel-content">
        <h3 class="title-hero title-hero-cad list-title">
            <span class="title-icon">@yield('title-icon')</span>@yield('title')
        </h3>
        @yield('optional-pass')
    </div>
    @yield('form-elements')
@stop