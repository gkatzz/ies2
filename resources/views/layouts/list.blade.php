@extends('layouts.master')
@include('includes.css.list')
@section('body')
<div class="panel">
    <div class="panel-body">
        <a type="button" href="@yield('url')" class="btn btn-primary pull-right">
            <i class="fa fa-plus-circle nbtn-new-icon"></i>Novo
        </a>
        <h3 class="title-hero list-title">
            <span class="title-icon">@yield('title-icon')</span>@yield('title')
        </h3>

        <div class="panel-content panel-list">
            <table id="datatable-responsive" data-tbl-class="@yield('data-tbl-class')" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        @yield('thead-columns')
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        @yield('tfoot-columns')
                    </tr>
                </tfoot>
                <tbody>
                    @yield('tbody-rows')
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalCreateSuccess">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@yield('modal-create-title') - Registro criado com sucesso!</h4>
            </div>
            <div class="modal-body text-center">
                <p>
                    Agora você pode atribuir novas funcionalidades ao registro criado.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-close-createsuccess" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalEditSuccess">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@yield('modal-edit-title') - Registro editado com sucesso!</h4>
            </div>
            <div class="modal-body text-center">
                <p>
                    Todas as informações do registro foram atualizadas.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-close-createsuccess" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalExclude">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Excluir @yield('modal-exclude-title')</h4>
            </div>
            <div class="modal-body text-center">
                <p id="title-exclude"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-gray btn-cancel-exclude" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary exclude">Confirmar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalExcludeSuccess">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@yield('modal-exclude-title') - Registro excluído com sucesso!</h4>
            </div>
            <div class="modal-body text-center">
                <p>
                    O registro selecionado foi removido do sistema.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-reload-exclude" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalError">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Ocorreu um erro ao editar o registro.</h4>
            </div>
            <div class="modal-body text-center">
                <p>
                    Entre em contato com o administrador para mais informações.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-close-createsuccess" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalPrice">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" id="myPleaseWait" tabindex="-1"
     role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-time"></span>Processando
                </h4>
            </div>
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-info progress-bar-striped active" style="width: 100%"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop