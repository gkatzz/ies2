@extends('layouts.master')
@section('home')
<div id="loading">
    <div class="center-vertical loader">
        <img src="{{asset('images/spinner.svg')}}">
    </div>
</div>
<div class="center-vertical bg-gray-login">
    <div class="center-content">
        @yield('content')
    </div>
</div>
@stop